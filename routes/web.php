<?php

$router->middleware([
    'before' => [
        'maintenance'
    ]
], function () use ($router) {
    $router->group('/user', function () use ($router) {
        $router->middleware([
            'before' => [
                'unloguedApp'
            ]
        ], function () use ($router) {
            $router->get('/login', 'LoginController::login');
        });
        $router->get('/logout', 'LoginController::logout');
    });
    $router->middleware([
        'before' => [
            'unloguedApp'
        ]
    ], function () use ($router) {
        $router->get('/oauth2/v2.0/authorize', 'LoginController::loginRequest');
    });
    
    $router->middleware([
        'before' => [
            'authApp',
            'databaseApp',
            'update'
        ]
    ], function () use ($router) {
        $router->get('/workspace/login', 'LoginController::loginWorkspace');
        $router->get('/', 'BackofficeController::index');
    });    
});