<?php

$router->group('/api', function () use ($router) {
    $router->middleware([
        'before' => [
            'maintenance'
        ]
    ], function () use ($router) {
        $router->get('/test', 'TestController::index')
            ->scope('api-test')
            ->description('API Rota de teste')
        ;
    });
});