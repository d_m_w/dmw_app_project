<?php

namespace App\Validation;

use Dmw\Core\Validator\Validation;
use Dmw\Core\Validator\Validate;
use Dmw\Core\Validator\Rules\Required;
use Dmw\Component\Contracts\Language\LangInterface;
use Dmw\Core\Html\FormMessage;

class LoginValidation extends Validation
{
    /**
     * @var Validate
     */
    private $validate;

    /**
     * @var FormMessage
     */
    private $formMessage;

    /**
     * @var array
     */
    private $data;

    /**
     * @param LangInterface $lang
     * @param FormMessage   $formMessage
     * @param array         $data
     */
    public function __construct(
        LangInterface $lang,
        FormMessage $formMessage,
        array $data
    ) {
        $this->data = $data;
        $this->formMessage = $formMessage;
        $this->validate = new Validate;
        $this->validate->addRule(new Required($lang, 'code', 'code'));
        $this->validate->addRule(new Required($lang, 'state', 'state'));
    }

    /**
     * @return Validate
     */
    protected function validate(): Validate
    {
        return $this->validate;
    }
    
    /**
     * @return FormMessage
     */
    protected function formMessage(): FormMessage
    {
        return $this->formMessage;
    }

    /**
     * @return array
     */
    protected function data(): array
    {
        return $this->data;
    }
}
