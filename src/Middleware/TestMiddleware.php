<?php

namespace App\Middleware;

use Dmw\Core\Routing\Middleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class TestMiddleware extends Middleware
{
    /**
     * @param  Request  $request
     * @return Response
     */
    public function handle(
        Request $request
    ): Response {
        return $this->response(self::HTTP_OK);
    }
}
