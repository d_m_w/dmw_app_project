<?php

namespace App;

use Dmw\App\Exception\MaintenanceException;
use Dmw\App\Service\LogService;
use Dmw\Core\Routing\Controller;
use Dmw\Core\Routing\Render;
use Dmw\Core\Kernel\Kernel as KernelBase;
use Dmw\Core\Console\Interfaces\SchedulerInterface;
use Dmw\Component\Contracts\Kernel\ContainerInterface;
use Dmw\Component\Contracts\Kernel\KernelExceptionInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class Kernel extends KernelBase
{
    /**
     * Lista de middlewares da pasta vendor
     * @var array
     */
    protected $middlewaresWeb = [
        'csrf' => \Dmw\Core\Sessions\Middlewares\CsrfMiddleware::class,
        'recaptcha' => \Dmw\Core\Captcha\Middlewares\ReCaptchaMiddleware::class,
        'language' => \Dmw\Component\Language\Middlewares\LanguageMiddleware::class,
        'oauth2' => \Dmw\Component\OAuth2\Middlewares\OAuth2Middleware::class,
        'uriSource' => \Dmw\App\Middleware\User\UriSourceMiddleware::class,
        'authOwner' => \Dmw\App\Middleware\User\AuthOwnerMiddleware::class,
        'authAdmin' => \Dmw\App\Middleware\User\AuthAdminMiddleware::class,
        'authApp' => \Dmw\App\Middleware\User\AuthAppMiddleware::class,
        'unloguedApp' => \Dmw\App\Middleware\User\UnloguedAppMiddleware::class,
        'databaseApp' => \Dmw\App\Middleware\Database\DatabaseAppMiddleware::class,
        'update' => \Dmw\App\Middleware\Database\UpdateAppMiddleware::class,
        'maintenance' => \Dmw\App\Middleware\MaintenanceMiddleware::class,
    ];

    /**
     * Lista de middlewares da pasta vendor
     * @var array
     */
    protected $middlewaresApi = [
        'databaseApi' => \Dmw\App\Middleware\Database\DatabaseApiMiddleware::class,
        'apiAuth' => \Dmw\App\Middleware\Api\AuthMiddleware::class,
        'maintenance' => \Dmw\App\Middleware\MaintenanceMiddleware::class,
    ];

    /**
     * Executa tarefas
     */
    public function schedule(SchedulerInterface $scheduler): void
    {
        //$scheduler->raw('app:command', ['--param' => 'value'])->everyMinute(5);
    }

    /**
     * Tratar excessões
     * @param ContainerInterface       $container
     * @param KernelExceptionInterface $kernelException
     */
    protected function onException(
        ContainerInterface $container,
        KernelExceptionInterface $kernelException
    ): void {
        $exception = $kernelException->getThrowable();
        $request = $container->get('request')->get();
        $errorCode = $this->setErrorCode($exception->getCode());

        if ($exception instanceof MaintenanceException) {
            $response = !$this->isApi($request) ? 
                $this->getViewResponse($container, '_messages/_maintenance') :
                $this->getHttpResponse($container, $request, $errorCode, $exception->getMessage())
            ;
            $kernelException->setResponse($response);
            return;
        }

        if ($exception->getCode() > 499 || $exception->getCode() < 200) {
            $this->resolve(LogService::class);

            $log = $container->get(LogService::class);
            $log->critical($exception->getMessage(), [], $exception);
        }

        if ($this->isApi($request) && $errorCode > 200) {
            $kernelException->setResponse($this->getHttpResponse(
                $container, 
                $request,
                $errorCode,
                $exception->getMessage()
            ));
        }
    }

    /**
     * Configura código de erro
     * @param int $code
     * @return int
     */
    private function setErrorCode(
        int $code
    ): int {
        $code = $code ?: 500;
        if ($code < 200 || $code > 599) {
            $code = 500;
        }

        return $code;
    }

    /**
     * Obtém renderização da view
     * @param ContainerInterface $container
     * @param string             $page
     * @return ResponseInterface
     */
    private function getViewResponse(
        ContainerInterface $container,
        string $page
    ): ResponseInterface {
        $controller = new Controller;
        $controller->setContainer($container);

        return $controller->view($page);
    }

    /**
     * Obtém resposta HTTP
     * @param ContainerInterface     $container
     * @param ServerRequestInterface $request
     * @param int                    $code
     * @param string                 $message
     * @return ResponseInterface
     */
    private function getHttpResponse(
        ContainerInterface $container,
        ServerRequestInterface $request,
        int $code,
        string $message
    ): ResponseInterface {
        $this->resolve(Render::class);

        $render = $container->get(Render::class);
        return $render->responseHttp($request, $message, $code < 600 ? $code : 500);
    }

    /**
     * Verifica se url é da API
     * @param ServerRequestInterface $request
     * @return bool
     */
    public function isApi(
        ServerRequestInterface $request
    ): bool {
        return substr($request->getUri()->getPath(), 0, 4) == '/api' ? true : false;
    }
}
