<?php

namespace App\Controller;

use Dmw\App\Entities\Page\PageEntity;
use Dmw\Component\Contracts\Language\LangInterface;
use Dmw\Core\Routing\Controller;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class BackofficeController extends Controller
{
    /**
     * @var string
     */
    private const ICON = '<i class="fa fa-user fa-lg"></i>';

    /**
     * @var LangInterface
     */
    private $lang;

    /**
     * @var PageEntity
     */
    private $page;

    /**
     * @param LangInterface $lang
     * @param PageEntity    $page
     */
    public function __construct(
        LangInterface $lang,
        PageEntity $page
    ) {
        $this->lang = $lang;
        $this->page = $page;
    }

    /**
     * @param  Request $request PSR7
     * @return Response
     */
    public function index(
        Request $request
    ): Response {
        //$this->model(Modell::class, true, 'db_app');

        $title = $this->lang->trans('Test');
        $description = $this->lang->trans('Test page');

        $this->page->configPage($title, $description);
        $this->page->configBackofficePage(
            $title,
            $description,
            self::ICON,
            [
                '<a href="/"><i class="fa fa-home fa-lg"></i></a>',
                $title
            ]
        );

        return $this->view('app/index', $this->page->getParams());
    }
}
