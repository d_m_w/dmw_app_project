<?php

namespace App\Controller;

use Exception;
use App\Validation\LoginValidation;
use Dmw\App\Service\Login\LoginAppService;
use Dmw\App\Service\Login\LoginService;
use Dmw\App\Traits\AppTrait;
use Dmw\Core\Html\FormMessage;
use Dmw\Core\Http\HttpHelper;
use Dmw\Core\Routing\Controller;
use Dmw\Component\Auth\Authenticate;
use Dmw\Component\Contracts\Language\LangInterface;
use Dmw\Component\Contracts\Configuration\ConfigInterface;
use Dmw\Client\Client;
use Dmw\Client\Storage\Session;
use Dmw\Client\Entities\CodeGrantEntity;
use Dmw\Client\Entities\AuthorizationCodeGrantEntity;
use Dmw\Client\Entities\ApiTokenEntity;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class LoginController extends Controller
{
    use AppTrait;

    /**
     * @var Authenticate
     */
    private $auth;

    /**
     * @var HttpHelper
     */
    private $http;
    
    /**
     * @var LangInterface
     */
    private $lang;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param HttpHelper    $http
     * @param LangInterface $lang
     */
    public function __construct(
        Authenticate $auth,
        HttpHelper $http,
        LangInterface $lang,
        ConfigInterface $config
    ) {
        $this->auth = $auth;
        $this->http = $http;
        $this->lang = $lang;
        $this->client = new Client(
            $config->get('app_data.client.id'),
            $config->get('app_data.client.secret'),
            new Session()
        );
    }

    /**
     * Obtem url de redirecionamento 
     * @return string
     */
    private function getRedirectUrl(): string
    {
        return $this->getAppUrl($this->config) . $this->config->get('app_data.redirect_uri');
    }

    /**
     * @param  Request $request PSR7
     * @return Response
     */
    public function login(
        Request $request
    ): Response {
        return $this->redirect(
            $this->client->oAuth()->codeUrl(
                (new CodeGrantEntity)
                    ->setRedirectUri($this->getRedirectUrl())
            )
        );
    }

    /**
     * @param Request      $request      PSR7
     * @param LoginService $loginService
     * @return Response
     */
    public function loginWorkspace(
        Request $request,
        LoginService $loginService
    ): Response {
        $loginService->unsetWorkspace();
        return $this->redirect(
            $this->client->oAuth()->codeUrl(
                (new CodeGrantEntity)
                    ->setRedirectUri($this->getRedirectUrl())
            )
        );
    }

    /**
     * Executa login
     * @param Request         $request
     * @param FormMessage     $formMessage
     * @param LoginAppService $loginService
     * @return Response
     */
    public function loginRequest(
        Request $request,
        FormMessage $formMessage,
        LoginAppService $loginService
    ): Response {
        $params = $this->http->getParams();
        $validator = new LoginValidation(
            $this->lang,
            $formMessage,
            $params
        );

        if (!$validator->make()->return()) {
            return $this->redirect('/user/login');
        }

        $token = $this->client->oAuth()->authorizationCode(
            (new AuthorizationCodeGrantEntity)
                ->setRedirectUri($this->getRedirectUrl())
                ->setCode($params['code'])
                ->setState($params['state'])
        );
        if (!$token instanceof ApiTokenEntity) {
            throw new Exception("Authentication failed");
        }

        $profile = $this->client->account()->profile();
        if (!$profile->workspace) {
            throw new Exception("Workspace not found");            
        }

        $loginService->setData(
            $profile->id,
            $profile->workspace->id,
            $profile->scopes
        );
        $loginService->setAppVersion(
            $profile->workspace->app->id,
            $profile->workspace->app->version
        );        

        $source = $this->auth->getUriSource(true);
        return $this->redirect($source ?: '/');
    }

    /**
     * Executa logout
     * @param  Request         $request Request PSR7
     * @param  HttpHelper      $helper
     * @param  LoginAppService $loginService
     * @return Response
     */
    public function logout(
        Request $request,
        HttpHelper $helper,
        LoginAppService $loginService
    ): Response {
        $params = $helper->setParams([
            'redirect_uri'
        ]);

        $this->auth->logout();
        $this->client->clearToken();
        $loginService->logout();
        $url = $params['redirect_uri'] ?: '/';

        return $this->redirect($url);
    }
}
