<?php

namespace App\Dpayment\Command;

use Dmw\Core\Console\Command;
use Dmw\Core\Kernel\Environment;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CodeSnifferFixCommand extends Command
{
    /**
     * @var string
     */
    protected $commandName = 'custom:code-sniffer-fix';

    /**
     * @var string
     */
    protected $commandDescription = "Execute and fix CodeSniffer";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Configuração do comando
     */
    protected function configure(): void
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
        ;
    }

    /**
     * Executa comando
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $io = new SymfonyStyle($input, $output);
        $process = new Process([
            Environment::pathVendor() . 'vendor/bin/phpcbf',
            'src',
            'config',
            'routes',
            '--standard=PSR1',
            '-p'
        ]);
        $process->run(function ($type, $buffer) use ($output): void {
            $output->write($buffer);
        });

        if (!$process->isSuccessful()) {
            $io->error('Falha ao executar comando');
            return Command::FAILURE;
        }

        $process = new Process([
            Environment::pathVendor() . 'vendor/bin/phpcbf',
            'src',
            'config',
            'routes',
            '--standard=PSR2',
            '-p'
        ]);
        $process->run(function ($type, $buffer) use ($output): void {
            $output->write($buffer);
        });

        if (!$process->isSuccessful()) {
            $io->error('Falha ao executar comando');
            return Command::FAILURE;
        }
       
        $io->success('Comando executado com sucesso');
        return Command::SUCCESS;
    }
}
