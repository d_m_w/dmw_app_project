<?php

require_once __DIR__ . "/../vendor/autoload.php";

use App\Kernel;
use Dmw\Core\Http\Request;

$request = new Request;
$kernel = new Kernel($request);
$response = $kernel->start();
$response->send();
$kernel->end();
