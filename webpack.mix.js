let mix = require('laravel-mix');

require('laravel-mix-sri');

mix.sass('resources/sass/main.scss', 'vendor/vali-admin/css/main.min.css')
   .sass('resources/sass/components/theme.scss', 'css/theme.min.css')
   .js('resources/js/main.js', 'vendor/vali-admin/js/main.min.js')
   .js('resources/js/app.js', 'js/app.min.js')
   .js('resources/js/components/forms.js', 'js/forms.min.js')
   .js('resources/js/components/theme.js', 'js/theme.min.js')
   .setPublicPath('public')
   .generateIntegrityHash()
;
