<?php

use Dmw\Component\Database\Migration;
use Phinx\Db\Adapter\MysqlAdapter;

final class Log extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        if ($this->hasTable('log')) {
            return;
        }

        $table = $this->table('log', [
            'id'          => false,
            'primary_key' => ['id']
        ]);

        $table
            ->addColumn('id', 'integer', ['identity' => true]) //Auto_increment
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP'
              ])
            ->addColumn('channel', 'string', ['limit' => 50])
            ->addColumn('level_name', 'string', ['limit' => 20])
            ->addColumn('message',  'string', ['limit' => 255])
            ->addColumn('context', 'text', [
                'limit' => MysqlAdapter::TEXT_LONG,
                'null' => true
            ])
            ->addColumn('extra', 'text', [
                'limit' => MysqlAdapter::TEXT_LONG,
                'null' => true
            ])
            ->addColumn('workspace_id', 'integer', [
                'null' => true
            ])
            ->addColumn('user_id', 'integer', [
                'null' => true
            ])
            ->create();
    }

    public function down(): void
    {
        $this->execute('DROP TABLE IF EXISTS log');
    }
}
