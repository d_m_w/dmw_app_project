<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'default' => [
            'driver' => 'mysql',
            'dsn' => Environment::env('DB_URL')
        ],
        'db_app' => [
            'driver' => 'mysql',
            'dsn' => '',
            'version' => '1.0'
        ],
        'redis' => [
            'driver' => 'redis',
            'dsn' => Environment::env('DB_REDIS')
        ]
    ],
    'schema' => Schema::create([
        'default' => Schema::array([
            'driver' => Schema::anyOf('mysql', 'sqlite', 'postgresql', 'oci8', 'firebird')->required(),
            'dsn' => Schema::string()->required()
        ]),
        'db_app' => Schema::array([
            'driver' => Schema::anyOf('mysql', 'sqlite', 'postgresql', 'oci8', 'firebird')->required(),
            'dsn' => Schema::string(),
            'version' => Schema::string()->required()
        ]),
        'redis' => Schema::create([
            'driver' => Schema::anyOf('redis')->required(),
            'dsn' => Schema::string()->required()
        ])
    ])
];
