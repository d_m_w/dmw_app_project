<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'driver' => 'smsapi',
        'drivers' => [
            'smsapi' => [
                'token' => Environment::env('SMS_SMSAPI_TOKEN')
            ]
        ]
    ],
    'schema' => Schema::create([
        'driver' => Schema::anyOf('smsapi'),
        'drivers' => Schema::array([
            'smsapi' => Schema::array([
                'token' => Schema::string(),
            ])
        ])
    ])
];
