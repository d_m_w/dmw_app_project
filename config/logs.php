<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'enabled' => true,
        'default' => Environment::env('LOGS_DEFAULT'),
        'chanell' => 'app-name',
        'handlers' => [
            'file' => [
                'handler' => \Dmw\Component\Logs\Handler\FileHandler::class
            ],
            'database' => [
                'handler' => \Dmw\Component\Logs\Handler\DatabaseHandler::class,
                'table_name' => 'log',
                'database' => 'default'
            ]
        ]
    ],
    'schema' => Schema::create([
        'enabled' => Schema::bool()->required(),
        'default' => Schema::anyOf('file', 'database')->required(),
        'chanell' => Schema::string()->required(),
        'handlers' => Schema::array([
            'file' => Schema::array([
                'handler' => Schema::string()->required()
            ]),
            'database' => Schema::array([
                'handler' => Schema::string()->required(),
                'table_name' => Schema::string()->required(),
                'database' => Schema::string()->required()
            ])
        ])
    ])
];
