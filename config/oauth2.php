<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'keys' => [
            'public' => Environment::env('OAUTH2_PUBLIC_KEY'),
            'private' => Environment::env('OAUTH2_PRIVATE_KEY'),
            'encryption' => Environment::env('OAUTH2_ENCRYPTION_KEY')
        ]
    ],
    'schema' => Schema::create([
        'keys' => Schema::array([
            'public' => Schema::string(),
            'private' => Schema::string(),
            'encryption' => Schema::string()
        ])
    ])
];
