<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'adapter' => 'sync',
        'queue_name' => 'queue',
        'adapters' => [
            'database' => [
                'database' => 'default',
                'table_name' => 'message',
                'attempts' => 3,
                'retry_after' => 1, //minutes
                'multiplier' => 3
            ],
            'redis' => [
                'database' => 'redis',
                'prefix' => '',
                'attempts' => 3,
                'retry_after' => 1, //minutes
                'multiplier' => 3
            ],
            'rabbitmq' => [
                'dsn' => Environment::env('QUEUE_RABBITMQ'),
                'attempts' => 3,
                'retry_after' => 1, //minutes
                'multiplier' => 3
            ]
        ],
        'failed' => [
            'table_name' => 'message_failed',
            'database' => 'default'
        ]
    ],
    'schema' => Schema::create([
        'adapter' => Schema::anyOf('database', 'redis', 'rabbitmq', 'sync')->required(),
        'queue_name' => Schema::string()->required(),
        'adapters' => Schema::array([
            'database' => Schema::array([
                'database' => Schema::string()->required(),
                'table_name' => Schema::string(),
                'attempts' => Schema::int(),
                'retry_after' => Schema::int(),
                'multiplier' => Schema::int()
            ]),
            'redis' => Schema::array([
                'database' => Schema::string()->required(),
                'prefix' => Schema::string(),
                'attempts' => Schema::int(),
                'retry_after' => Schema::int(),
                'multiplier' => Schema::int()
            ]),
            'rabbitmq' => Schema::array([
                'dsn' => Schema::string()->required(),
                'attempts' => Schema::int(),
                'retry_after' => Schema::int(),
                'multiplier' => Schema::int()
            ])
        ]),
        'failed' => Schema::array([
            'table_name' => Schema::string(),
            'database' => Schema::string()
        ])
    ])
];
