<?php

use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'prefix' => 'dmw_cache',
        'driver' => 'symfony',
        'adapter' => 'file',
        'adapters' => [
            'file' => [
                'path' => null
            ],
            'redis' => [
                'database' => 'redis'
            ]
        ]
    ],
    'schema' => Schema::create([
        'prefix' => Schema::string()->required(),
        'driver' => Schema::anyOf('symfony', 'phpfast')->required(),
        'adapter' => Schema::anyOf('file', 'redis'),
        'adapters' => Schema::array([
            'file' => Schema::array([
                'path' => Schema::string()->nullable()
            ]),
            'redis' => Schema::array([
                'database' => Schema::anyOf('redis')->required(),
            ])
        ])
    ])
];
