<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'web_push' => [
            'private_key' => Environment::env('PUSH_WEBPUSH_PRIVATE_KEY'),
            'public_key' => Environment::env('PUSH_WEBPUSH_PUBLIC_KEY')
        ]
    ],
    'schema' => Schema::create([
        'web_push' => Schema::array([
            'private_key' => Schema::string(),
            'public_key' => Schema::string()
        ])
    ])
];
