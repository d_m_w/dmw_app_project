<?php

use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'status_page' => [
            'url' => 'https://dmw.statuspage.io/'
        ]
    ],
    'schema' => Schema::create([
        'status_page' => Schema::array([
            'url' => Schema::string()->required()
        ])
    ])
];