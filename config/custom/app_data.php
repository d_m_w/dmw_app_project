<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'maintenance' => false,
        'limit' => [
            'workspaces' => null
        ],
        'url' => [
            'dev' => 'http://apps.dmw.test/',
            'staging' => 'https://dmw.net.br/apps/',
            'prod' => 'https://dmw.net.br/apps/'
        ],
        'account_url' => [
            'dev' => 'http://user.dmw.test/',
            'staging' => 'http://user.dmw.test/',
            'prod' => 'https://dmw.net.br/account/'
        ],        
        'platform_url' => [
            'dev' => 'http://apps.dmw.test/',
            'staging' => 'https://dmw.net.br/apps/',
            'prod' => 'https://dmw.net.br/apps/'
        ],        
        'client' => [
            'id' => Environment::env('CLIENT_ID'),
            'secret' => Environment::env('CLIENT_SECRET'),
        ],
        'redirect_uri' => 'oauth2/v2.0/authorize'
    ],
    'schema' => Schema::create([
        'maintenance' => Schema::bool(),
        'limit' => Schema::array([
            'workspaces' => Schema::int()->nullable()
        ]),
        'url' => Schema::array([
            'dev' => Schema::string()->required(),
            'staging' => Schema::string()->required(),
            'prod' => Schema::string()->required()
        ]),
        'account_url' => Schema::array([
            'dev' => Schema::string()->required(),
            'staging' => Schema::string()->required(),
            'prod' => Schema::string()->required()
        ]),        
        'platform_url' => Schema::array([
            'dev' => Schema::string()->required(),
            'staging' => Schema::string()->required(),
            'prod' => Schema::string()->required()
        ]),
        'client' => Schema::array([
            'id' => Schema::string()->required(),
            'secret' => Schema::string()->required()
        ]),
        'redirect_uri' => Schema::string()->required()
    ])
];