<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'assets' => [
            'bootstrap' => [
                'js' => [
                    '<script src=\'/vendor/bootstrap/js/bootstrap.min.js\'></script>'
                ]
            ],
            'd3js' => [
                'js' => [
                    '<script type=\'text/javascript\' src=\'/vendor/d3js/js/d3.v3.min.js\'></script>',
                    '<script type=\'text/javascript\' src=\'/vendor/d3js/js/Donut3D.js\'></script>'
                ]
            ],
            'fontawesome' => [
                'css' => [
                    '<link rel=\'stylesheet\' href=\'https://use.fontawesome.com/releases/v5.2.0/css/all.css\' integrity=\'sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ\' crossorigin=\'anonymous\'>'
                ]
            ],
            'googleoauth' => [
                'js' => [
                    '<script src=\'https://apis.google.com/js/platform.js\' async defer></script>'
                ]
            ],
            'googleCharts' => [
                'js' => [
                    '<script type=\'text/javascript\' src=\'/vendor/googleCharts/js/loader.js\'></script>'
                ]
            ],
            'jump' => [
                'js' => [
                    '<script src=\'/vendor/jump/js/jump.min.js\'></script>'
                ]
            ],
            'jquery' => [
                'js' => [
                    '<script src=\'/vendor/jquery/js/jquery-3.5.1.min.js\'></script>'
                ]
            ],
            'pace' => [
                'js' => [
                    '<script src=\'/vendor/pace/js/pace.min.js\'></script>'
                ]
            ],
            'popper' => [
                'js' => [
                    '<script src=\'/vendor/popper/js/popper.min.js\'></script>'
                ]
            ],
            'swiper' => [
                'css' => [
                    '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/swiper/css/swiper.min.css\'>'
                ],
                'js' => [
                    '<script src=\'/vendor/swiper/js/swiper.min.js\'></script>'
                ]
            ],
            'tinymce' => [
                'js' => [
                    '<script src=\'https://cloud.tinymce.com/5/tinymce.min.js?apiKey=rwm7y8tv4hrlq9k1wm7z8di3q1h7d7jzyzes8r2dzut55kea\'></script>'
                ]
            ],
            'recaptcha' => [
                'js' => [
                    '<script src=\'https://www.google.com/recaptcha/api.js\'></script>'
                ]
            ],
            'vali-admin' => [
                'css' => [
                    'default' => [
                        'color' => '#b85252',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.min.css\'>'
                    ],
                    'blue' => [
                        'color' => '#2196F3',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.blue.min.css\'>'
                    ],
                    'blue-grey' => [
                        'color' => '#546E7A',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.blue-grey.min.css\'>'
                    ],
                    'brown' => [
                        'color' => '#795548',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.brown.min.css\'>'
                    ],
                    'green' => [
                        'color' => '#4CAF50',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.green.min.css\'>'
                    ],
                    'pink' => [
                        'color' => '#F06292',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.pink.min.css\'>'
                    ],
                    'purple' => [
                        'color' => '#673AB7',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.purple.min.css\'>'
                    ],
                    'teal' => [
                        'color' => '#009688',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.teal.min.css\'>'
                    ],
                    'dark' => [
                      'color' => '#455a64',
                      'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.dark.min.css\'>'
                    ],
                    'black' => [
                        'color' => '#222D32',
                        'file' => '<link rel=\'stylesheet\' type=\'text/css\' href=\'/vendor/vali-admin/css/main.black.min.css\'>'
                    ],                    
                ],
                'js' => [
                    '<script src=\'/vendor/vali-admin/js/main.min.js\'></script>'
                ]
            ]
        ],
        'tags' => [
            'title' => 'Website',
            'description' => 'Website description',
            'keywords' => 'Website, site',
            'theme-color' => '#b85252'
        ],
        'og-tags' => [
            'siteName' => 'Website',
            'title' => 'Website description',
            'image' => 'https://website.com/images/logo.png',
            'uri' => 'https://website.com',
            'type' => 'website',
            'locale' => 'pt_BR'
        ],
        'forms' => [
            'tinymce_api_key' => Environment::env('FORMS_TINYMCE_API_KEY')
        ],
        'google-gtm-code' => Environment::env('GOOGLE_GTM_CODE')
    ],
    'schema' => Schema::create([
        'assets' => Schema::array([
            'bootstrap' => Schema::array([
                'js' => Schema::array([
                ])
            ]),
            'd3js' => Schema::array([
                'js' => Schema::array([])
            ]),
            'fontawesome' => Schema::array([
                'css' => Schema::array([])
            ]),
            'googleoauth' => Schema::array([
                'js' => Schema::array([])
            ]),
            'googleCharts' => Schema::array([
                'js' => Schema::array([])
            ]),
            'jump' => Schema::array([
                'js' => Schema::array([])
            ]),
            'jquery' => Schema::array([
                'js' => Schema::array([])
            ]),
            'pace' => Schema::array([
                'js' => Schema::array([])
            ]),
            'popper' => Schema::array([
                'js' => Schema::array([])
            ]),
            'swiper' => Schema::array([
                'css' => Schema::array([]),
                'js' => Schema::array([])
            ]),
            'tinymce' => Schema::array([
                'js' => Schema::array([])
            ]),
            'recaptcha' => Schema::array([
                'js' => Schema::array([])
            ]),
            'vali-admin' => Schema::array([
                'css' => Schema::array([
                    'default' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'blue' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'blue-grey' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'brown' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'green' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'pink' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'purple' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'teal' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'dark' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),
                    'black' => Schema::array([
                        'color' => Schema::string()->required(),
                        'file' => Schema::string()->required()
                    ]),                    
                ]),
                'js' => Schema::array([
                ])
            ])
        ]),
        'tags' => Schema::array([
            'title' => Schema::string()->required(),
            'description' => Schema::string()->required(),
            'keywords' => Schema::string()->required(),
            'theme-color' => Schema::string()->required()
        ]),
        'og-tags' => Schema::array([
            'siteName' => Schema::string()->required(),
            'title' => Schema::string()->required(),
            'image' => Schema::string()->required(),
            'uri' => Schema::string()->required(),
            'type' => Schema::string()->required(),
            'locale' => Schema::string()->required()
        ]),
        'forms' => Schema::array([
            'tinymce_api_key' => Schema::string()
        ]),
        'google-gtm-code' => Schema::string()
    ])
];
