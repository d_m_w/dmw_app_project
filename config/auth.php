<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'session_name' => 'dmw_auth',
        'method' => 'ARGON2I',
        'table' => 'usuario',
        'fields' => [
            'id' => 'id',
            'user' => 'usuario',
            'pass' => 'senha',
            'enabled' => 'ativo'
        ],
        'policy' => [
            'privacy' => '/policy/privacy',
            'terms' => '/policy/service'
        ],
        'google' => [
            'enabled' => true,
            'client_id' => Environment::env('AUTH_GOOGLE_CLIENT_ID'),
            'client_secret' => Environment::env('AUTH_GOOGLE_CLIENT_SECRET'),
            'redirect_uri' => '',
            'hosted_domain' => null //Não obrigadtório. Domínio
        ],
        'facebook' => [
            'enabled' => false,
            'app_id' => Environment::env('AUTH_FACEBOOK_APP_ID'),
            'app_secret' => Environment::env('AUTH_FACEBOOK_APP_SECRET'),
            'redirect_uri' => '',
            'graph_api_version' => 'v2.10'
        ],
        'microsoft' => [
            'enabled' => false,
            'client_id' => Environment::env('AUTH_MICROSOFT_CLIENT_ID'),
            'client_secret' => Environment::env('AUTH_MICROSOFT_CLIENT_SECRET'),
            'redirect_uri' => ''
        ]
    ],
    'schema' => Schema::create([
        'session_name' => Schema::string()->required(),
        'method' => Schema::anyOf('ARGON2I', 'AES_ENCRYPT')->required(),
        'table' => Schema::string()->required(),
        'fields' => Schema::array([
            'id' => Schema::string()->required(),
            'user' => Schema::string()->required(),
            'pass' => Schema::string()->required(),
            'enabled' => Schema::string()->required()
        ]),
        'policy' => Schema::array([
            'privacy' => Schema::string()->required(),
            'terms' => Schema::string()->required()
        ]),
        'google' => Schema::array([
            'enabled' => Schema::bool()->required(),
            'client_id' => Schema::string()->required(),
            'client_secret' => Schema::string()->required(),
            'redirect_uri' => Schema::string()->required(),
            'hosted_domain' => Schema::string()->nullable()
        ]),
        'facebook' => Schema::array([
            'enabled' => Schema::bool()->required(),
            'app_id' => Schema::string()->required(),
            'app_secret' => Schema::string()->required(),
            'redirect_uri' => Schema::string()->required(),
            'graph_api_version' => Schema::string()->required()
        ]),
        'microsoft' => Schema::array([
            'enabled' => Schema::bool()->required(),
            'client_id' => Schema::string()->required(),
            'client_secret' => Schema::string()->required(),
            'redirect_uri' => Schema::string()->required()
        ])
    ])
];
