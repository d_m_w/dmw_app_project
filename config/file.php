<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'ftp' => [
            'protocol' => 'ftp',
            'url' => Environment::env('FTP_URL')
        ],
        'sftp' => [
            'protocol' => 'sftp',
            'url' => Environment::env('SFTP_URL')
        ]
    ],
    'schema' => Schema::create([
        'ftp' => Schema::array([
            'protocol' => Schema::anyOf('ftp')->required(),
            'url' => Schema::strings()
        ]),
        'sftp' => Schema::array([
            'protocol' => Schema::anyOf('sftp')->required(),
            'url' => Schema::string()
        ])
    ])
];
