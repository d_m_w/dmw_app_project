export const formatNumber = (value, decimalSize = 2) =>
{
    if (!value) {
        value = 0;
    }

  /*
  value = parseFloat(value).toFixed(decimalSize);
  value = value.replace('.', ',');

  return value;
  */
    let val = (value/1).toFixed(decimalSize).replace('.', ',');

    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

export const round = (value, precision) =>
{
    var factor = Math.pow(10, precision);
    var tempNumber = value * factor;
    var roundedTempNumber = Math.round(tempNumber);

    return roundedTempNumber / factor;
}

export const onlyNumbers = (value) =>
{
    value = value.replace(/[^\d]+/g,'');

    return value;
}
