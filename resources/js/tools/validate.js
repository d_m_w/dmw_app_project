export const cpf = (value) =>
{
    let add;
    let rev;
    let i;
  
    value = value.replace(/[^\d]+/g,'');

    if (value == '') {
        return false;
    }

    if (value.length != 11 ||
        value == "00000000000" ||
        value == "11111111111" ||
        value == "22222222222" ||
        value == "33333333333" ||
        value == "44444444444" ||
        value == "55555555555" ||
        value == "66666666666" ||
        value == "77777777777" ||
        value == "88888888888" ||
        value == "99999999999") {
        return false;
    }

    add = 0;

    for (i=0; i < 9; i ++) {
        add += parseInt(value.charAt(i)) * (10 - i);
    }

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11) {
        rev = 0;
    }

    if (rev != parseInt(value.charAt(9))) {
        return false;
    }

    add = 0;

    for (i = 0; i < 10; i ++) {
        add += parseInt(value.charAt(i)) * (11 - i);
    }

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11) {
        rev = 0;
    }

    if (rev != parseInt(value.charAt(10))) {
        return false;
    }

    return true;
}

export const cnpj = (value) =>
{
    value = value.replace(/[^\d]+/g,'');

    if (value == '') {
        return false;
    }

    if (value.length != 14) {
        return false;
    }

    if (value == "00000000000000" ||
      value == "11111111111111" ||
      value == "22222222222222" ||
      value == "33333333333333" ||
      value == "44444444444444" ||
      value == "55555555555555" ||
      value == "66666666666666" ||
      value == "77777777777777" ||
      value == "88888888888888" ||
      value == "99999999999999") {
        return false;
    }

    size    = value.length - 2
    numbers = value.substring(0, size);
    digits  = value.substring(size);
    sum     = 0;
    pos     = size - 7;

    for (i = size; i >= 1; i--) {
        sum += numbers.charAt(size - i) * pos--;

        if (pos < 2) {
            pos = 9;
        }
    }

    result = sum % 11 < 2 ? 0 : 11 - sum % 11;

    if (result != digits.charAt(0)) {
        return false;
    }

    size    = size + 1;
    numbers = value.substring(0, size);
    sum     = 0;
    pos     = size - 7;

    for (i = size; i >= 1; i--) {
        sum += numbers.charAt(size - i) * pos--;

        if (pos < 2) {
            pos = 9;
        }
    }

    result = sum % 11 < 2 ? 0 : 11 - sum % 11;

    if (result != digits.charAt(1)) {
        return false;
    }

    return true;
}

export const email = (value) =>
{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
        return (true);
    }

    return (false);
}

export const cep = (value) =>
{
    var new_cep    = value.replace(/\D/g, '');
    var validation = /^[0-9]{8}$/;

    if (validation.test(new_cep)) {
        return true;
    } else {
        return false;
    }
}
