export const removeSpaces = (value) =>
{
    return value.replace(/\s/g, '');
}

export const removeAccentuation = (value) =>
{
    return value.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
}
