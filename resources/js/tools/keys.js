export const getDmw = (assistant = false) =>
{
    if (assistant) {
        return 'c8ko88w80kg8k4s0o4swock80cgc44wc4g4sk8w8';
    } else {
        return 'ks8w00sko0c8ccgwco0s84o0oocsc0g4s480w8g8';
    }
};

export const facebook = () => { app_id: '500400767033343' };

export const webPush = () => 'BCGELzDPXsdBad1WEbG7rgBqlxYGLgQdHcfwIGSfPtF2vA5ItBPR0_kyX8pxiXu4o_XMl2PibCMzKfRlCnvXQAo';

export const getMercadoPago = (sandbox = false) =>
{
    if (sandbox) {
        return 'TEST-a595e668-3270-4e3f-8e86-9f2c98d46e67';
    } else {
        return 'APP_USR-c74c34a9-22e1-4262-913d-2fb671b1fd9f';
    }
};
