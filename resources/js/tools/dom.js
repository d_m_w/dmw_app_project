const setElement = (element) => (typeof element !== 'undefined') ? element : null;

export const get = (element, context = document) => setElement(context.querySelector(element));

export const getAll = (element, context = document) => setElement(context.querySelectorAll(element));

export const getId = (element, context = document) => setElement(context.getElementById(element));

export const getClass = (element, context = document) => setElement(context.getElementsByClassName(element));

export const createElement = (element, content = document) => content.createElement(element);

export const getByName = (element, context = document) => context.getElementsByName(element);

export const ready = (fn) =>
{
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

export const currentUrl = () =>
{
    return document.URL;
}

export const redirect = (url) =>
{
    url = decodeURI(url);

    window.location.href = url;
}

export const buildUrlQuery = (obj) =>
{
    return Object.entries(obj).map(([key, val]) => `${key}=${val}`).join('&');
}
