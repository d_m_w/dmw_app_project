//synth  = window.speechSynthesis;

export const play = (synth) =>
{
    synth.resume();
}

export const pause = (synth) =>
{
    synth.pause();
}

export const playPause = (synth) =>
{
    if (synth.paused) {
        play(synth);
    } else {
        pause(synth);
    }
}

export const stop = (synth) =>
{
    synth.cancel();
}

export const load = (synth, voices, text) =>
{
    if (voices) {
        stop(synth);
    
        let utterThis = new SpeechSynthesisUtterance(text);

        for (let i = 0; i < voices.length; i++) {
            if (voices[i].default) {
                utterThis.voice = voices[i];
                break;
            }
        }

        utterThis.rate  = 1.2;
        utterThis.pitch = 1;

        synth.speak(utterThis);
    } else {
        console.log('Falha ao carregar lista de vozes!');
    }
}
