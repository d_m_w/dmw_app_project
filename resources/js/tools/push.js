let keyPublic = 'BNojFlyb9yrzfn_bWCp6WskmJNHJwTZlXd4TqOBT6Mpe6w1dCfBk45PIfMxFzZFm_jTag-vjWnYKDqFBGdK5SaM';

let isSubscribed = false;

const pushButton = document.querySelector('.js-push-btn');

function urlB64ToUint8Array(base64String)
{
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64  = (base64String + padding)
                    .replace(/\-/g, '+')
                    .replace(/_/g, '/');

    const rawData     = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }

    return outputArray;
}

export const initialiseUI = (swRegistration) =>
{
    if (pushButton) {
        pushButton.addEventListener('click', function () {
            pushButton.disabled = true;

            if (isSubscribed) {
                unsubscribeUser(swRegistration);
            } else {
                subscribeUser(swRegistration);
            }
        });
    }

  // Set the initial subscription value
    swRegistration.pushManager.getSubscription()
    .then(function (subscription) {
        isSubscribed = !(subscription === null);

        updateSubscriptionOnServer(subscription);

        if (isSubscribed) {
            console.log('User IS subscribed.');
        } else {
            console.log('User is NOT subscribed.');
        }

        updateBtn();
    });
}

function updateBtn()
{
    if (pushButton) {
        if (Notification.permission === 'denied') {
            pushButton.textContent = 'Push Messaging Blocked.';
            pushButton.disabled    = true;

            updateSubscriptionOnServer(null);

            return;
        }

        if (isSubscribed) {
            pushButton.textContent = 'Disable Push Messaging';
        } else {
            pushButton.textContent = 'Enable Push Messaging';
        }

        pushButton.disabled = false;
    }
}

function subscribeUser(swRegistration)
{
    const applicationServerKey = urlB64ToUint8Array(keyPublic);

    swRegistration.pushManager.subscribe(
        {
            userVisibleOnly: true,
            applicationServerKey: applicationServerKey
        }
    )
    .then(function (subscription) {
        console.log('User is subscribed:', subscription);

        updateSubscriptionOnServer(subscription);

        isSubscribed = true;

        updateBtn();
    })
    .catch(function (err) {
        console.log('Failed to subscribe the user: ', err);

        updateBtn();
    });
}

function updateSubscriptionOnServer(subscription)
{
  // TODO: Send subscription to application server

    const subscriptionJson    = document.querySelector('.js-subscription-json');
    const subscriptionDetails = document.querySelector('.js-subscription-details');

    if (subscription) {
        subscriptionJson.textContent = JSON.stringify(subscription);
        subscriptionDetails.classList.remove('is-invisible');
    } else if (subscriptionDetails) {
        subscriptionDetails.classList.add('is-invisible');
    }
}

function unsubscribeUser(swRegistration)
{
    swRegistration.pushManager.getSubscription()
    .then(function (subscription) {
        if (subscription) {
            return subscription.unsubscribe();
        }
    })
    .catch(function (error) {
        console.log('Error unsubscribing', error);
    })
    .then(function () {
        updateSubscriptionOnServer(null);

        console.log('User is unsubscribed.');
        isSubscribed = false;

        updateBtn();
    });
}
