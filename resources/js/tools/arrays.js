export const remove = (data, index) => data.slice(index);

export const sum = (data, column) =>
{
    let result = 0;

    data.forEach(
        (elem) =>
        {
            result += parseFloat(elem[column]);
        }
    );

  return result;
}

export const isObject = (arr) =>
{
    return Array.isArray(arr) === 'undefined' ? false : true;
}

export const isEmpty = (obj) =>
{
    return Object.keys(obj).length === 0;
}
