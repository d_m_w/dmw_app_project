import Swal from 'sweetalert2';

/*
type => warning, error, success, info e question
*/

export const message = (type, text) =>
{
    const toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2500
    });

  toast.fire({
        type: type,
        title: text
    });
}

export const messageClick = (type, title, text = '', footer = '') =>
{
    return Swal.fire({
        title: title,
        text: text,
        footer: footer,
        type: type,
        showCancelButton: false,
        confirmButtonText: 'Ok'
    });
}

export const messageBox = (type, title, text = '', footer = '') =>
{
    Swal.fire({
        type: type,
        title: title,
        text: text,
        footer: footer
    });
}

export const question = (type, title, text = '', footer = '') =>
{
    return Swal.fire({
        title: title,
        text: text,
        footer: footer,
        type: type,
        showCancelButton: true,
        confirmButtonText: 'Sim',
        cancelButtonText: 'Não'
    });
}

export const questionResponse = (result) =>
{
    if (result.value) {
        return true;
    } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.close();
        return;
    }
}
