import axios from 'axios';

export const http = (url) =>
{
    return axios.create({
        baseURL: url,
        timeout: 10000
    });
}
