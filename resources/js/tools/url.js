export const getCurrentUrl = () =>
{
    let protocol = window.location.protocol;
    let hostname = window.location.hostname;

    return `${protocol}//${hostname}/`;
}
