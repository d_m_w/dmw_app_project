import { get } from '../tools/dom';
import jqueryMaskPlugin from 'jquery-mask-plugin';
import $ from 'jquery';

const setKeyboardMoney = (inputName) =>
{
    let comp = get(inputName);

    if (comp) {
        comp.type = 'tel';
    }
}

export const cnpj = (inputName) =>
{
    setKeyboardMoney(inputName);

    $(inputName).mask('00.000.000/0000-00');
}

export const cpf = (inputName) =>
{
    setKeyboardMoney(inputName);

    $(inputName).mask('000.000.000-00');
}

export const cep = (inputName) =>
{
    setKeyboardMoney(inputName);

    $(inputName).mask('00000-000');
}

export const ip = (inputName) =>
{
    setKeyboardMoney(inputName);

    $(inputName).mask('0ZZ.0ZZ.0ZZ.0ZZ', {
        translation: {
            'Z': {
                pattern: /[0-9]/, optional: true
            }
        }
    });
}

export const cvv = (inputName) =>
{
    $(inputName).mask('000');
}

export const yearMonth = (inputName) =>
{
    $(inputName).mask('00/00');
}

export const creditCard = (inputName) =>
{
    $(inputName).mask('0000 0000 0000 0000 000');
}

export const phone = (inputName) =>
{
    inputName.type = 'tel';

    var maskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(maskBehavior.apply({}, arguments), options);
        }
    };

    $(inputName).mask(maskBehavior, options);
}

export const mail = (inputName) =>
{
    let comp = get(inputName);

    if (comp) {
        comp.type = 'email';
    }
}

export const money = (inputName) =>
{
    setKeyboardMoney(inputName);

    $(inputName).mask('#.##0,00', {reverse: true});
}

export const custom = (inputName, mask) =>
{
    setKeyboardMoney(inputName);

    if (mask) {
        $(inputName).mask(mask, {reverse: true});
    }
}

export const number = (inputName) =>
{
    setKeyboardMoney(inputName);

    $(inputName).mask('000000000000000', {reverse: true});
}

export const value = (inputName, money = false) =>
{
    let value = $(inputName).cleanVal();

    if (money) {
        return value / 100;
    } else {
        return value;
    }
}
