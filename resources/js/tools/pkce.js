const base64urlEncode = (str) =>
{
    return str.toString('base64')
              .replace(/\+/g, '-')
              .replace(/\//g, '_')
              .replace(/=/g, '');
}

// Dependency: Node.js crypto module
// https://nodejs.org/api/crypto.html#crypto_crypto
const sha256 = (buffer) =>
{
    return crypto.createHash('sha256').update(buffer).digest();
}

// Dependency: Node.js crypto module
// https://nodejs.org/api/crypto.html#crypto_crypto
export const random = () =>
{
    return crypto.randomBytes(32);
}

export const getVerifier = (random) =>
{
    return base64URLEncode(random);
}

export const getChallenge = (verifier) =>
{
    return base64URLEncode(sha256(verifier));
}
