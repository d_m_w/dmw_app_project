export const getCookie = (name) =>
{
    let cookie = RegExp("" + name + "[^;]+").exec(document.cookie);

    return decodeURIComponent(!!cookie ? cookie.toString().replace(/^[^=]+./, "") : "");
}

export const setCookie = (
    name,
    value,
    expires = null, //Em minutos
    domain = null,
    path = null,
    secure = null,
    httpOnly = null,
    sameSite = 'Lax') =>
{
    let str = `${name}=${value};`;

    if (expires) {
        let now = Date.now();
        let dateExpire = new Date(now + expires * 60000);
        dateExpire = dateExpire.toGMTString();

        str += ` Expires=${dateExpire};`;
    }

    if (domain) {
        str += ` Domain=${domain};`;
    }

    if (path) {
        str += ` Path=${path};`;
    }

    if (secure) {
        str += ` Secure;`;
    }

    if (httpOnly) {
        str += ` HttpOnly;`;
    }

    if (sameSite) {
        str += ` SameSite=${sameSite};`;
    }

    document.cookie = str;
}
