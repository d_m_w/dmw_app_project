import axios from 'axios';

export const http = (url, token) =>
{
    return axios.create({
        baseURL: url,
        timeout: 10000,
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        }
    });
}

export const setResponse = (error) =>
{
    let response = error.response;
    if (!response) {
        return error;
    }

    let data = response.data;
    return data ? data : response;
}
