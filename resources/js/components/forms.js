import * as dom from '../tools/dom';
import * as notify from '../tools/notify';
import * as arrays from '../tools/arrays';

/////////////////////////////////////////////////////////////////////////FUNÇÕES
export const setLoading = (hidden = false, cssClass = '.overlay') =>
{
    let loading = dom.getAll(cssClass);

    if (loading) {
        loading.forEach(
            (elem) =>
            {
                elem.hidden = hidden;
            }
        );
    }
}

export const submit = (e, btnSubmit) => {
    let key = e.which || e.keyCode;

    if (key == 13) {
        btnSubmit.click();
    }
}

export const submitGet = (formName, url) =>
{
    let form = dom.getId(formName);

    if (form) {
        let inputs = form.elements;

        if (inputs) {
            let params = [];

            for (let key in inputs) {
                let input = inputs[key];

                if (input) {
                    let nodeName = input.nodeName;

                    if (
                    (nodeName == 'INPUT') ||
                    (nodeName == 'SELECT') ||
                    (nodeName == 'TEXTAREA')
                    ) {
                        let type = input.type;
                        let nam  = input.name;
                        let val  = input.value;

                        if (type.toLowerCase() === 'checkbox') {
                            if (input.checked) {
                                params[nam] = val;
                            } else {
                                params[nam] = '';
                            }
                        } else {
                            params[nam] = val;
                        }
                    }
                }
            }

            let uri = dom.buildUrlQuery(params);

            dom.redirect(`${url}${uri}`);
        } else {
            console.log('Falha ao obter elementos do form');
        }
    } else {
        console.log('Form não localizado');
    }
}

export const clear = (formName) =>
{
    let form = dom.getId(formName);

    if (form) {
        let inputs = form.elements;

        if (inputs) {
            for (let key in inputs) {
                let input = inputs[key];

                if (input) {
                    let nodeName = input.nodeName;

                    if (
                    (nodeName == 'INPUT') ||
                    (nodeName == 'SELECT') ||
                    (nodeName == 'TEXTAREA')
                    ) {
                        if (nodeName == 'SELECT') {
                            let clas = input.getAttribute('class');

                            if (!clas || clas.indexOf('select2') < 0) {
                                //Select2 tratar separado para não precisar importar
                                let type = input.type;

                                if (type.toLowerCase() === 'checkbox') {
                                    input.checked = false;
                                } else {
                                    input.value = '';
                                }
                            }
                        } else {
                            let type = input.type;

                            if (type.toLowerCase() === 'checkbox') {
                                input.checked = false;
                            } else {
                                input.value = '';
                            }
                        }
                    }
                }
            }
        } else {
            console.log('Falha ao obter elementos do form');
        }
    } else {
        console.log('Form não localizado');
    }
}

export const menuSelHidden = (hidden) =>
{
    let menuSel = dom.getAll('.menuSel');

    if (menuSel) {
        menuSel.forEach(
            (elem) =>
            {
                elem.hidden = hidden;
            }
        );
    }
}

export const tableRowClick = () =>
{
    let table = dom.get('.tableRow');

    if (table) {
        let rows = table.rows;

        if (rows) {
            table.classList.add('table-hover');

            for (let row of rows) {
                let cells = row.cells;
                let ckSel = dom.get('.sel', row);

                if (ckSel) { //Adiciona a seleção na tabela
                    ckSel.addEventListener(
                        'click',
                        () =>
                        {
                            if (ckSel.checked) {
                                row.classList.add("table-active");

                                menuSelHidden(false);
                            } else {
                                row.classList.remove("table-active");

                                menuSelHidden(true);
                            }
                        }
                    );
                }

                for (let i = 0; i < cells.length; i++) {
                    if (i > 0) {
                        if (cells[i].tagName != 'TH') {
                            if (cells[i].getAttribute('data-click') !== 'no') {
                                cells[i].style.cursor = 'pointer';

                                cells[i].addEventListener(
                                    'click',
                                    () =>
                                    {
                                        let url = row.getAttribute('data-url');

                                        if (url) {
                                            dom.redirect(url);
                                        }
                                    }
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}

export const setFormErrors = (errors) =>
{
    let err = errors.errors ? errors.errors : errors;

    if (arrays.isObject(err)) {
        Object.keys(err).forEach((key) => {
            let value     = err[key];
            let component = dom.get(`#${key}`);

            if (component) {
                setInputError(`#${key}`, value);
            }
        });

        notify.messageBox('error', 'Falha ao validar formulário!');
    } else {
        notify.messageBox('error', err);
    }
}

export const setInputError = (id, message) =>
{
    let element = dom.get(id);

    if (element) {
        let parent = element.parentElement;

        if (message) {
            if (parent) {
                let panelMsg = parent.getElementsByClassName('invalid-feedback');

                if (panelMsg) {
                    panelMsg[0].innerHTML = message;
                }
            }

            element.classList.add('is-invalid');
        } else {
            if (parent) {
                let panelMsg = parent.getElementsByClassName('invalid-feedback');

                if (panelMsg && panelMsg.length > 0) {
                    panelMsg[0].innerHTML = '';
                }
            }

            element.classList.remove('is-invalid');
        }
    }
}

export const exclui = (elem) =>
{
    let url = elem.getAttribute('data-url');
    let txt = elem.getAttribute('data-text');

    if (url) {
        let title = 'Deseja excluir?';
        let text  = txt ? txt : 'Para restaurar vá em itens excluídos!';
        let type  = 'question';

        notify.question(type, title, text)
        .then(
            result => {
            if (notify.questionResponse(result)) {
                let token = dom.getByName('_token')[0];
                let form = document.createElement('form');
                form.method = 'post';
                form.action = url;

                let hiddenField1 = document.createElement('input');
                hiddenField1.type = 'hidden';
                hiddenField1.name = '_method';
                hiddenField1.value = 'delete';

                form.appendChild(hiddenField1);

                let hiddenField2 = document.createElement('input');
                hiddenField2.type = 'hidden';
                hiddenField2.name = '_token';
                hiddenField2.value = token.value;

                form.appendChild(hiddenField2);

                document.body.appendChild(form);

                form.submit();
            }
            }
        )
        .catch(
            error => {
            console.log(error);
            }
        );
    }
}

/////////////////////////////////////////////////////////////////////////EVENTOS
dom.ready(function () {
    menuSelHidden(true);
    tableRowClick();
});

const formSubmit = dom.getAll('.formSubmit');

if (formSubmit) {
    formSubmit.forEach(
        (elem) =>
        {
            elem.addEventListener(
                'submit',
                () =>
                {
                    let btnSubmit = dom.get('.btnSubmit');

                    if (btnSubmit) {
                        let text = btnSubmit.textContent;

                        btnSubmit.innerHTML = `<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>${text} `;
                        btnSubmit.disabled  = true;
                    }
                }
            );
        }
    );
}

const btnRemove = dom.getAll('.btnRemove');

if (btnRemove) {
    btnRemove.forEach(
        (elem) =>
        {
            elem.addEventListener(
                'click',
                () =>
                {
                    exclui(elem);
                }
            );
        }
    );
}

const btnRestore = dom.getAll('.btnRestore');

if (btnRestore) {
    btnRestore.forEach(
        (elem) => {
        elem.addEventListener(
                'click',
                () => {
                let url = elem.getAttribute('data-url');
                let txt = elem.getAttribute('data-text');
                if (url) {
                    let title = 'Deseja restaurar?';
                    let text  = txt ? txt : 'Após restaurar, este item estará novamente disponível!';
                    let type  = 'question';

                    notify.question(type, title, text)
                    .then(
                    result => {
                        if (notify.questionResponse(result)) {
                            let token = dom.getByName('_token')[0];
                            let form = document.createElement('form');
                            form.method = 'post';
                            form.action = url;
                
                            let hiddenField = document.createElement('input');
                            hiddenField.type = 'hidden';
                            hiddenField.name = '_token';
                            hiddenField.value = token.value;
                
                            form.appendChild(hiddenField);
                
                            document.body.appendChild(form);
                
                            form.submit();
                        }
                        }
                    )
                    .catch(
                        error => {
                            console.log(error);
                            }
                    );
                }
                }
            );
        }
    );
}

const btnSearch = dom.get('.btnSearch');

if (btnSearch) {
    btnSearch.addEventListener(
        'focus',
        () =>
        {
            btnSearch.value = '';
        }
    );
}

const btnGroupDrop = dom.get('#btnGroupDrop');

if (btnGroupDrop) {
    btnGroupDrop.addEventListener(
        'click',
        () =>
        {
            $('html,body').animate(
                {
                    scrollTop: $('.tile').offset().top
                },
                'slow',
                function () {
                    $('.dropdown-toggle').dropdown();
                }
            );
        }
    );
}
