import * as dom from '../tools/dom';
import * as mask from  '../tools/mask';
import { messageBox } from '../tools/notify';
import { setInputError } from '../components/forms';

dom.ready(function () {
    mask.number('#recurrCount');
});

$('#frmRecurrency').on('hidden.bs.modal', function (e) {
    setInputError('#recurrCount', '');
    setInputError('#recurrFrequency', '');
    setInputError('#recurrFrequencyFixed', '');
    setInputError('#recurrUntil', '');
})

const setRecurrency = (parcelas, recorrencia, termino) =>
{
    return {
        parcelas: parcelas,
        recorrencia: recorrencia,
        termino: termino
    };
}

let recurrBtnVoltar = dom.get('#recurrBtnVoltar');

if (recurrBtnVoltar) {
    recurrBtnVoltar.addEventListener(
        'click',
        () =>
        {
            let recurrency = dom.get('#recurrency');

            recurrency.value = '';

            clear();

            $('#frmRecurrency').modal('hide');
        }
    );
}

let recurrBtnSave = dom.get('#recurrBtnSave');

if (recurrBtnSave) {
    recurrBtnSave.addEventListener(
        'click',
        () =>
        {
            let installTab = dom.get('#nav-installments-tab');
            let fixedTab   = dom.get('#nav-fixed-tab');
            let data       = {};

            if (installTab.getAttribute('aria-selected') == 'true') { //Parcelamento
                let countError     = '';
                let frequencyError = '';
                let count          = dom.get('#recurrCount').value;
                let frequency      = dom.get('#recurrFrequency').value;

                if (count < 2) {
                    countError = 'Quantidade de parcelas deve ser superior a 2!';
                } else if (count > 500) {
                    countError = 'Quantidade de parcelas deve ser inferior a 500!';
                }

                if (!frequency) {
                    frequencyError = 'Informe o intervalo!';
                }

                setInputError('#recurrCount', countError);
                setInputError('#recurrFrequency', frequencyError);

                if (countError || frequencyError) {
                    return;
                }

                data = setRecurrency(dom.get('#recurrCount').value, dom.get('#recurrFrequency').value, '');
            }

            if (fixedTab.getAttribute('aria-selected') == 'true') { //Fixo
                let frequencyFixedError = '';
                let untilError          = '';
                let frequencyFixed      = dom.get('#recurrFrequencyFixed').value;
                let until               = dom.get('#recurrUntil').value;

                if (!frequencyFixed) {
                    frequencyFixedError = 'Informe o intervalo!';
                }

                if (!until) {
                    untilError = 'Informe uma data de término!';
                }

                setInputError('#recurrFrequencyFixed', frequencyFixedError);
                setInputError('#recurrUntil', untilError);

                if (frequencyFixedError || untilError) {
                    return;
                }

                data = setRecurrency(0, frequencyFixed, until);
            }

            dom.get('#recurrency').value = JSON.stringify(data);

            $('#frmRecurrency').modal('hide');
        }
    );
}

export const clear = () =>
{
    dom.get('#recurrCount').value          = '';
    dom.get('#recurrFrequency').value      = '';
    dom.get('#recurrFrequencyFixed').value = '';
    dom.get('#recurrUntil').value          = '';
}

export const get = () =>
{
    let res = dom.get('#recurrency').value;

    return res ? JSON.parse(res) : '';
}
