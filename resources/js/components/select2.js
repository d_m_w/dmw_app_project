import $ from 'jquery';
import select2 from 'select2';

//Linguagem pt-BR => select2/dist/js/!18n/pt-BR.js
!function () {
    if ($&&$.fn&&$.fn.select2&&$.fn.select2.amd) {
        var e=$.fn.select2.amd;
    }e.define("select2/i18n/pt-BR",[],function () {
        return{
            errorLoading: function () {
                return "Os resultados não puderam ser carregados."
            }, 
            inputTooLong: function (e) {
                var n=e.input.length-e.maximum,r="Apague "+n+" caracter";
                return 1!=n&&(r+="es"),r
            },
            inputTooShort:function (e) {
                return "Digite "+(e.minimum-e.input.length) + " ou mais caracteres"
            },
            loadingMore:function () {
                return"Carregando mais resultados…"
            },
            maximumSelected:function (e) {
                var n="Você só pode selecionar "+e.maximum+" ite";
                return 1==e.maximum?n+="m":n+="ns",n
            },
            noResults:function () {
                return"Nenhum resultado encontrado"
            },
            searching:function () {
                return"Buscando…"
            },
            removeAllItems:function () {
                return"Remover todos os itens"
            }
        }
    }),
    e.define,e.require
}();

export const clear = (componentName) => {
    $(componentName).val(null).trigger('change');
}

export const setSelectedMultiple = (componentName, arrayOfValues) => {
    $(componentName).val(arrayOfValues).trigger('change');
}

export const setSelected = (componentName, key, value) => {
    $(componentName).select2("trigger", "select", {
        data: { id: key, nome: value }
    });
}

export const dataResult = (componentName) => {
    let value = $(componentName).val();
    return value ? $(componentName).select2('data') : '';
}

export const indexName = (componentName) =>
{
    let data = dataResult(componentName);
    let id   = '';
    let name = '';
    
    if (!data) {
        return {id: '', name: ''};
    }

    data.forEach(function (elem) {
        id   = elem.id;
        name = elem.nome;
    });

    return {id: id, name: name};
}

export const formatState = (state) => {
    if (!state.id) {
        return state.text;
    }

    var $state = $('<span><i class="' + $(state.element).attr('data-icon') + ' fa-lg"></i> ' + state.text + '</span>');

    return $state;
};

export const simple = (componentName, componentPlaceHolder = null) => {
    let allowClear = componentPlaceHolder ? true : false;

    $(componentName).select2({
        width: "100%",
        tags: "true",
        placeholder: componentPlaceHolder,
        allowClear: allowClear});
}

export const ajax = (
    componentName,
    componentPlaceHolder,
    url,
    callbackParams = null,
    callbackResult = null,
    callbackSelection = null,
    valueId = null,
    valueName = null,
    windowParent = null
) => {
    let allowClear = componentPlaceHolder ? true : false;
    let a          = 1;

    if (!componentName) {
        console.log('Parâmetro "componentName" não informado!');
        return false;
    }

    if (!url) {
        console.log('Parâmetro "url" não informado!');
        return false;
    }

    var functionResult;
    var functionSelection;

    functionResult = callbackResult != null ? callbackResult : result;
    functionSelection = callbackSelection != null ? callbackSelection : selection;

    $(componentName).select2({
        minimumInputLength: 2,
        dropdownParent: windowParent ? $(windowParent) : null,
        amdLanguageBase: "pt-BR",
        language: "pt-BR",
        width: "100%",
        multiple: false,
        ajax: {
            url: url,
            type: "GET",
            dataType: "json",
            delay: 300,
            data: function (params) {
                var param = {
                    nome: `%${params.term}%`
                }

                if (callbackParams != null) {
                    param = callbackParams(param);
                }

                return param;
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            },
            cache: true
        },
        placeholder: componentPlaceHolder,
        allowClear: allowClear,
        escapeMarkup: function (markup) {
            return markup; 
        },
        templateResult: functionResult,
        templateSelection: functionSelection
    });

    if (valueName && valueId) {
        setSelected(componentName, valueId, valueName);
    }
}

export const result = (repo) =>
{
    return repo.loading ? repo.text : repo.nome;
}

export const selection = (repo) =>
{
    return repo.nome ? repo.nome : repo.text;
}
