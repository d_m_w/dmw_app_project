import * as dom from '../tools/dom';
import * as notify from '../tools/notify';

export const copyToClipboard = (input) =>
{
    if (input) {
        let type = input.type;

        if (type && type == 'password') {
            navigator.clipboard.writeText(input.value);
        } else {
            input.select();
        }

        document.execCommand("copy");

        notify.message("info", "Texto copiado para a área de transferência");
    }
}
