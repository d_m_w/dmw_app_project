import * as dom from '../tools/dom';

export const getAllChecked = (componentClass) =>
{
    let sel = dom.getAll(componentClass);
    let res = [];

    if (sel.length) {
        sel.forEach(
            (elem) =>
            {
                if (elem.checked) {
                    res.push(elem);
                }
            }
        );
    }

    return res;
}
