const setResult = (result, value = '') =>
{
    return {
        result: result,
        value: value
    }
}

export const itemFocus = (component) =>
{
    if (component) {
        component.focus();
    }
}

export const itemChange = (index, items, required = []) =>
{
    let count  = items.length - 1;
    let add    = true;
    let remove = false;

    for (let i = 0; i < required.length; i++) {
        let itm = items[index];
        let idx = required[i];

        if (!itm[idx]) {
            add = false;
            break;
        }
    }

    if (add) {
        return itemAdd(items, required);
    }

    for (let j = 0; j < required.length + 1; j++) {
        let itm = items[index];
        let idx = required[j];

        if (!itm[idx]) {
            remove = true;
            break;
        }
    }

    if (remove) {
        return itemRemove(index + 1, items);
    }

    return setResult(false, '');
}

export const itemAdd = (items, required = []) =>
{
    for (let i = 0; i < items.length; i++) {
        if (i + 1 == items.length) {
            if (required.length > 0) {
                for (let j = 0; j < required.length; j++) {
                    let campo = required[j];
                    let its   = items[i];

                    if (!its[campo]) {
                        return setResult(false, campo);
                    }
                }
            }
        }
    }

    return setResult(true);
}

export const itemRemove = (index, items) =>
{
    let newItems = [];

    if (items.length > 1) {
        for (let i = 0; i < items.length; i++) {
            if (i != index) {
                newItems.push(items[i]);
            }
        }
    } else {
        newItems = items;
    }

    return setResult(true, newItems);
}
