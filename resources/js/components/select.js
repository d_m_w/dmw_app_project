import * as dom from '../tools/dom';

export const getKey = (componentId) =>
{
    let comp = dom.get(componentId);

    return comp && comp.selectedIndex != -1 ? comp[comp.selectedIndex].value : '';
}

export const getText = (componentId) =>
{
    let comp = dom.get(componentId);

    return comp && comp.selectedIndex != -1 ? comp[comp.selectedIndex].text : '';
}

export const getValue = (componentId) =>
{
    let comp = dom.get(componentId);

    if (comp.multiple) {
        let res = [];

        if (comp.selectedOptions) {
            let options = comp.selectedOptions;

            for (let i = 0; i < options.length; i++) {
                res.push(options[i].value);
            }

            return res;
        }
    } else {
        return comp.value;
    }
}

export const setValue = (componentId, value) =>
{
    let comp = dom.get(componentId);

    if (comp.multiple) {
        let options = comp.options;

        if (options.length > 0) {
            for (let i = 0; i < options.length; i++) {
                if (value.includes(options[i].value)) {
                    options[i].selected = true;
                }
            }
        }
    } else {
        comp.value = value;
    }
}

export const clearOptions = (componentId) =>
{
    let comp = dom.get(componentId);

    if (comp) {
        let options = comp.selectedOptions;

        for (let i = 0; i < options.length; i++) {
            console.log(i);
            comp.remove(i);
        }
    }
}

export const setOptions = (params, list, value = null) =>
{
    if (params) {
        let comp = dom.get(params.name);

        if (comp) {
            if (list) {
                let compKey = params.key;
                let compVal = params.value;

                for (let key in list) {
                    let option = dom.createElement('option');

                    if (compKey && compVal) {
                        option.value = list[key][compKey];
                        option.text  = list[key][compVal];
                    } else {
                        option.value = key;
                        option.text  = list[key];
                    }

                    comp.add(option, null);
                }

                if (value) {
                    comp.value = value;
                }
            } else {
                console.log('Lista não definida');
            }
        } else {
            console.log('Componente não localizado');
        }
    } else {
        console.log('Parâmetros não localizados');
    }
}
