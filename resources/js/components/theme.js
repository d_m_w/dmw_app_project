import * as dom from '../tools/dom';

const themeList = dom.getAll('.theme-button');

if (themeList) {
    themeList.forEach((elem) => {
        elem.addEventListener('click', function () {
                let color = elem.getAttribute('data-color');
                let theme = elem.getAttribute('data-theme');

                themeList.forEach((e) => {
                        e.classList.remove('theme-button-select');
                    });

                elem.classList.add('theme-button-select');
                dom.get('#color').value = color;
                dom.get('#theme').value = theme;
        });
    });
}
