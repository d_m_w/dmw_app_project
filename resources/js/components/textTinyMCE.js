export const setValue = (element, value) =>
{
  //tinymce.get(element).setContent(value);
    $(element).html(value);
}

export const getValue = (element) =>
{
    return tinymce.get(element).getContent();
}

export const textareaTinymce = (element) =>
{
    tinymce.init({
        selector:element,
        height: 250,
        menubar: false,
        domLoaded: true,
        browser_spellcheck: true,
        language_url : '/vendor/tinymce/js/pt_BR.js',
        language: 'pt_BR',
        theme: 'silver',
        mobile: {
            theme: 'mobile',
            plugins: [
            'autosave', 'lists'
            ],
            toolbar: [
            'bold', 'italic', 'underline', 'bullist', 'numlist', 'removeformat'
            ]
        },
        plugins: [
        'advlist autolink lists link image charmap print preview',
        'searchreplace visualblocks code fullscreen',
        'media table paste code help wordcount fullscreen'
        ],
        toolbar: [
        'bold italic underline strikethrough fontsizeselect | table | insert | bullist numlist | searchreplace | removeformat | fullscreen'
        ]
    });
}
