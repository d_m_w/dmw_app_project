import * as url from './tools/url';
import * as dom from './tools/dom';
import * as http from './tools/http';
import * as webpush from './tools/webpush';

let isSubscribed   = false;
let swRegistration = null;

let pnNotify      = dom.get("#pnNotify");
let btnNotify     = dom.get("#btnNotify");
let btnNotifyList = dom.get("#btnNotifyList");

const getPublicKey = () =>
{
    return http.get(`push/public/key`);
}

const insertValue = (params) =>
{
    return http.post(`push/endpoint/insere`, params);
}

const removeValue = (params) =>
{
    return http.post(`push/endpoint/remove`, params);
}

const showPanelNotify = (show) =>
{
    if (pnNotify && btnNotifyList) {
        pnNotify.style.display      = show ? 'inline' : 'none';
        btnNotifyList.style.display = show ? 'none' : 'inline';
    }
};

const updateSubscriptionOnServer = (subscription, remove = false) =>
{
    let endpoint = JSON.stringify(subscription);

    if (endpoint && endpoint != 'null') {
        let script = remove ? removeValue : insertValue;

        script(
            {
                'nome': endpoint
            }
        )
        .then(
            response =>
            {

            }
        )
        .catch(
            error =>
            {
                console.error(error);
            }
        );
    }
}

const subscribeUser = () =>
{
    getPublicKey()
    .then(
        response =>
        {
            let publicKey            = response.data;
            let applicationServerKey = webpush.urlB64ToUint8Array(publicKey);

            swRegistration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: applicationServerKey
            })
        .then(
            subscription =>
            {
                updateSubscriptionOnServer(subscription);

                isSubscribed = true;

                showPanelNotify(!isSubscribed);
            }
        )
        .catch(
            err =>
            {
                console.error('Falha ao inscrever usuário: ', err);

                showPanelNotify(!isSubscribed);
            }
        );
        }
    )
    .catch(
        error =>
        {
            console.error(error);
        }
    );
}

const unsubscribeUser = () =>
{
    swRegistration.pushManager.getSubscription()
    .then(
        subscription =>
        {
            if (subscription) {
                return subscription.unsubscribe();
            }
        }
    )
  .catch(
      error =>
      {
            console.error('Falha ao descadastrar', error);
      }
  )
  .then(
      subscription =>
      {
            if (subscription) {
                updateSubscriptionOnServer(subscription, true);

                isSubscribed = false;
            }
      }
  );
}

const initialiseUI = () =>
{
    btnNotify.addEventListener('click', function () {
        btnNotify.disabled = true;

        if (isSubscribed) {
            showPanelNotify(false);
        } else {
            subscribeUser();
        }
    });

  swRegistration.pushManager.getSubscription()
  .then(function (subscription) {
    isSubscribed = !(subscription === null);

    if (!isSubscribed) {
        updateSubscriptionOnServer(subscription);
    }

    showPanelNotify(!isSubscribed);
  });
}

if ('serviceWorker' in navigator) {
    let host = url.getCurrentUrl();

    navigator.serviceWorker.register(`${host}service-worker.js`, {scope: '/'})
    .then(function (swReg) {
        if (btnNotify) {
            if ('PushManager' in window) {
                swRegistration = swReg;

                initialiseUI();
            } else {
                showPanelNotify(false);

                console.error('Tecnologia Push não é suportada');
            }
        }
    })
    .catch(function (error) {
        showPanelNotify(false);

        console.error('Falha ao registrar o Service Worker', error);
    });
} else {
    if (pnNotify) {
        showPanelNotify(false);
    }
}
